<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <title>Document</title>
</head>

<body>
    <header class="header-choice">
        <h1>SHORTEST PATH</h1>
    </header>

    <h1 class="grid-choice-title">Choisissez une grille</h1>

    <form action="result.php" method="GET" class="grid-form">
        <h2 class="grid-choice-subtitle">Grilles carrés et rectangulaires</h2>
        <div class="grid-choices-container">
            <div class="grid-choice">
                grille par défaut <br />
                8 obstacles <br />
                départ en (2, 4) <br />
                arrivée en (1, 0) <br />
                <input type="radio" value="default" id="grid-choice-1" name="grid" checked>
            </div>
            <div class="grid-choice">
                grille 4x4 <br />
                4 obstacles <br />
                départ en (0, 0) <br />
                arrivée en (1, 3) <br />
                <input type="radio" value="1" id="grid-choice-1" name="grid">
            </div>
            <div class="grid-choice">
                grille 6x4 <br />
                8 obstacles <br />
                départ en (2, 3) <br />
                arrivée en (1, 0) <br />
                <input type="radio" value="2" id="grid-choice-2" name="grid">
            </div>
            <div class="grid-choice">
                grille 5x6 <br />
                10 obstacles <br />
                départ en (4, 2) <br />
                arrivée en (1, 2) <br />
                <input type="radio" value="3" id="grid-choice-3" name="grid">
            </div>
            <div class="grid-choice">
                grille 7x7 <br />
                13 obstacles <br />
                départ en (5, 6) <br />
                arrivée en (3, 3) <br />
                <input type="radio" value="4" id="grid-choice-4" name="grid">
            </div>
        </div>

        <h2 class="grid-choice-subtitle">Grilles avec des formes spécifiques</h2>
        <div class="grid-choices-container">
            <div class="grid-choice">
                triangle <br />
                7 obstacles <br />
                départ en (5, 4) <br />
                arrivée en (2, 1) <br />
                <input type="radio" value="triangle" id="grid-choice-1" name="grid">
            </div>
            <div class="grid-choice">
                flèche <br />
                6 obstacles <br />
                départ en (6, 0) <br />
                arrivée en (3, 1) <br />
                <input type="radio" value="arrow" id="grid-choice-1" name="grid">
            </div>
        </div>
        <h2 class="grid-choice-subtitle">Grilles invalide</h2>
        <div class="grid-choices-container">
            <div class="grid-choice" id="grid-error-1">
                Pas de chemin <br />
                5 obstacles <br />
                départ en (0, 0) <br />
                arrivée en (4, 4) <br />
                <input type="radio" value="noPath" id="grid-choice-1" name="grid">
            </div>
            <div class="grid-choice" id="grid-error-2">
                Même points <br />
                6 obstacles <br />
                départ en (2, 2) <br />
                arrivée en (2, 2) <br />
                <input type="radio" value="samePoint" id="grid-choice-1" name="grid">
            </div>
        </div>
        <div class="grid-choices-submit">
            <button type="submit">Valider</button>
        </div>
    </form>
</body>

</html>