<?php

class Dijkstra
{
    private array $shortestPath;

    public function __construct()
    {
        $this->shortestPath = [];
    }

    public function findShortestPath(array $map, Box $currentPoint, Box $end, array $currentPath)
    {
        $currentPath[] = $currentPoint;

        if (!empty($this->shortestPath) && count($currentPath) >= count($this->shortestPath)) {
            return;
        }

        if ($currentPoint->getRole() === "end") {
            $this->setShortestPath($currentPath);
            return;
        }

        $points = [
            $map[$currentPoint->getY() - 1][$currentPoint->getX()] ?? null,
            $map[$currentPoint->getY() + 1][$currentPoint->getX()] ?? null,
            $map[$currentPoint->getY()][$currentPoint->getX() - 1] ?? null,
            $map[$currentPoint->getY()][$currentPoint->getX() + 1] ?? null,
        ];

        foreach ($points as $point) {
            if ($point !== null) {
                if ($map[$point->getY()][$point->getX()]->getRole() === "blocked") {
                    continue;
                }

                if (in_array($point, $currentPath)) {
                    continue;
                }

                $this->findShortestPath($map, $point, $end, $currentPath);
            }
        }
    }

    public function getShortestPath(): array
    {
        return $this->shortestPath;
    }

    public function setShortestPath(array $shortestPath): void
    {
        $this->shortestPath = $shortestPath;
    }
}
