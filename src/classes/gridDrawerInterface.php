<?php

interface GridDrawerInterface
{
    public function draw(): void;
    public function drawShortestPath(): void;
}