<?php

class Box
{

    private $x;
    private $y;
    private $role;

    public function __construct(int $x, int $y, string $role)
    {
        $this->x = $x;
        $this->y = $y;
        $this->role = $role;
    }

    public function getX() {
        return $this->x;
    }

    public function getY() {
        return $this->y;
    }
    public function getRole() {
        return $this->role;
    }

    public function setRole(String $role) {
        return $this->role = $role;
    }
}
