<?php

require_once("box.php");
require_once("GridDrawerInterface.php");
require_once("dijkstra.php");

class Grid implements GridDrawerInterface
{

    private $grid;
    private $start;
    private $end;
    private $gridWithShortestPath;
    private $shortestPathLength;

    public function __construct(array $grid, array $start, array $end)
    {
        $this->start = $this->transforEntries($start, "start"); //convert the array $start into a Box object
        $this->end = $this->transforEntries($end, "end"); //convert the array $end into a Box object
        $this->grid = $this->transformGrid($grid);
        $this->gridWithShortestPath = [];
        $this->shortestPathLength = -1;
    }

    public function transforEntries(array $entry, String $role)
    {
        $instance = new Box($entry[1], $entry[0], $role);
        return $instance;
    }

    public function transformGrid(array $grid)
    {


        for ($c = 0; $c < count($grid); $c++) {
            for ($l = 0; $l < count($grid[$c]); $l++) {
                $role = "";
                if ($l === $this->start->getX() && $c === $this->start->getY()) {
                    $role = "start";
                } else if ($l === $this->end->getX() && $c === $this->end->getY()) {
                    $role = "end";
                } else if ($grid[$c][$l] === 0) {
                    $role = "blocked";
                } else if ($grid[$c][$l] === 1) {
                    $role = "empty";
                }
                $grid[$c][$l] = new Box($l, $c, $role);
            }
        };
        return $grid;
    }

    public function draw(): void
    {
        $grid = $this->grid;

        echo "<table>";
        for ($c = 0; $c < count($grid); $c++) {
            echo "<tr>";
            for ($l = 0; $l < count($grid[$c]); $l++) {
                $character = "";
                if ($grid[$c][$l]->getRole() === "start") {
                    $character = "D";
                } else if ($grid[$c][$l]->getRole() === "end") {
                    $character = "A";
                } else if ($grid[$c][$l]->getRole() === "blocked") {
                    $character = "x";
                }

                echo "<td>$character</td>";
            }
            echo "</tr>";
        };
        echo "</table>";
    }

    public function solveShortestPath(Dijkstra $solver)
    {
        $solver->findShortestPath($this->grid, $this->start, $this->end, []);
        $shortestPath = $solver->getShortestPath();
        $gridShortestPath = array_map(
            function ($col) use ($shortestPath) {
                return array_map(
                    function ($itemInRow) use ($shortestPath) {
                        for ($i = 0; $i < count($shortestPath); $i++) {
                            if (
                                $itemInRow->getX() === $shortestPath[$i]->getX() &&
                                $itemInRow->getY() === $shortestPath[$i]->getY() &&
                                $itemInRow->getRole() !== "start" &&
                                $itemInRow->getRole() !== "end"
                            ) {
                                $itemInRow->setRole("shortest_path");
                            }
                        }

                        return $itemInRow;
                    },
                    $col
                );
            },
            $this->grid
        );

        $this->setShortestPathLength(count($shortestPath) - 1);
        $this->setGridWithShortestPath($gridShortestPath);
    }

    public function drawShortestPath(): void
    {
        $gridShortestPath = $this->getGridWithShortestPath();

        echo "<table>";
        for ($c = 0; $c < count($gridShortestPath); $c++) {
            echo "<tr>";
            for ($l = 0; $l < count($gridShortestPath[$c]); $l++) {
                $character = "";
                if ($gridShortestPath[$c][$l]->getRole() === "start") {
                    $character = "D";
                } else if ($gridShortestPath[$c][$l]->getRole() === "end") {
                    $character = "A";
                } else if ($gridShortestPath[$c][$l]->getRole() === "blocked") {
                    $character = "x";
                } else if ($gridShortestPath[$c][$l]->getRole() === "shortest_path") {
                    $character = "o";
                }

                echo "<td>$character</td>";
            }
            echo "</tr>";
        };
        echo "</table>";
    }

    public function getShortestPathLength(): int
    {
        return $this->shortestPathLength;
    }

    public function setShortestPathLength(int $shortestPathLength): void
    {
        $this->shortestPathLength = $shortestPathLength;
    }

    public function getGridWithShortestPath(): array
    {
        return $this->gridWithShortestPath;
    }

    public function setGridWithShortestPath(array $gridWithShortestPath): void
    {
        $this->gridWithShortestPath = $gridWithShortestPath;
    }
}
