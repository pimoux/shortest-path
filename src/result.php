<?php

require_once("classes/grid.php");
require_once("classes/dijkstra.php");

$param = null;
$availableParams = [1 => "default", 2 => "1", 3 => "2",  4 => "3", 5 => "4", 6 => "triangle", 7 => "arrow", 8 => "noPath", 9 => "samePoint"];
$paramError = false;
if (!isset($_GET["grid"])) {
    $paramError = true;
} else {
    $param = $_GET["grid"];
    $titleDisplay = [
        "default" => " par défaut",
        "1" => " numéro 1",
        "2" => " numéro 2",
        "3" => " numéro 3",
        "4" => " numéro 4",
        "triangle" => " triangulaire",
        "arrow" => " flèche",
        "noPath" => " avec aucun chemin possible",
        "samePoint" => " avec les même coordonnées de départ et d'arrivée",
    ];
}

if (!array_search($param, $availableParams)) {
    $paramError = true;
}

$grids = [
    "default" => [
        [1, 1, 1, 1, 1],
        [1, 1, 0, 0, 1],
        [0, 1, 1, 0, 1],
        [0, 1, 1, 0, 1],
        [1, 1, 0, 1, 1],
        [1, 1, 1, 1, 0],
    ],
    "1" => [
        [1, 0, 1, 1],
        [1, 1, 1, 0],
        [0, 1, 1, 1],
        [0, 1, 1, 1],
    ],
    "2" => [
        [0, 0, 1, 1, 1, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 0, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1],
    ],
    "3" => [
        [1, 0, 1, 1, 1],
        [1, 0, 1, 0, 0],
        [1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 1, 1],
        [0, 1, 1, 1, 0],
    ],
    "4" => [
        [0, 1, 1, 1, 1, 0, 0],
        [0, 1, 1, 1, 1, 1, 1],
        [1, 0, 1, 1, 1, 1, 1],
        [1, 0, 1, 1, 0, 0, 1],
        [1, 1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1, 1],
        [1, 0, 1, 0, 1, 0, 1],
    ],
    "triangle" => [
        [0],
        [1, 0],
        [0, 1, 0],
        [1, 1, 1, 0],
        [1, 0, 1, 1, 0],
        [1, 1, 1, 1, 1, 1],
    ],
    "arrow" => [
        [1],
        [0, 1, 1],
        [1, 1, 1, 1, 1],
        [0, 1, 0, 0, 1, 1, 0],
        [1, 0, 1, 1, 1],
        [1, 1, 1],
        [0],
    ],
    "noPath" => [
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 0, 0, 0],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 1, 1],
    ],
    "samePoint" => [
        [0, 1, 1, 1, 0],
        [1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1],
        [1, 0, 1, 1, 1],
        [1, 1, 0, 1, 1],
    ]
];

$starts = [
    "default" => [2, 4],
    "1" => [0, 0],
    "2" => [2, 3],
    "3" => [4, 2],
    "4" => [5, 6],
    "triangle" => [5, 4],
    "arrow" => [6, 0],
    "noPath" => [0, 0],
    "samePoint" => [2, 2],
];

$ends = [
    "default" => [1, 0],
    "1" => [1, 3],
    "2" => [1, 0],
    "3" => [1, 2],
    "4" => [3, 3],
    "triangle" => [2, 1],
    "arrow" => [3, 1],
    "noPath" => [4, 4],
    "samePoint" => [2, 2],
];


if (!$paramError) {
    $grid = new Grid($grids[$param], $starts[$param], $ends[$param]);
    $grid->solveShortestPath(new Dijkstra());
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style/style.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Document</title>
</head>

<body>
    <header class="header">
        <a href="index.php">
            <i class="fa fa-arrow-left" style="font-size:2em"></i>
        </a>
        <h1>SHORTEST PATH</h1>
    </header>
    <?php if (!$paramError) : ?>
        <section class="grids">
            <div class="grid-default-display">
                <h2>Voici l'affichage de votre grille.</h2>
                <h3>Vous avez choisi la grille <?= $titleDisplay[$param] ?></h3>
                <?php $grid->draw() ?>
            </div>
            <div class="grid-result-display">
                <?php if ($grid->getShortestPathLength() === -1) : ?>
                    <h3>Une erreur est survenue avec votre grille, avez-vous pensé à mettre des points de départ et d'arrivée différents ? Est-ce que vous avez fait en sorte que la grille puisse bien avoir au moins 1 chemin possible ?</h3>
                <?php else : ?>
                    <h2>Voici l'affichage de la grille avec le chemin le plus court.</h2>
                    <h3>Nombre d'étapes: <?= $grid->getShortestPathLength() ?></h3>
                    <?php $grid->drawShortestPath(); ?>
                <?php endif; ?>
            </div>
        </section>
    <?php else : ?>
        <h3 class="param-error">Avez-vous pensé à renseigner un paramètre ? Si oui, est-il valide ?</h3>
    <?php endif; ?>
</body>

</html>