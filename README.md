## **How to test the project ?**

-You need to clone this repository on your local machine: ```git clone https://gitlab.com/pimoux/shortest-path.git``` <br />
-To build the php apache environment, you need to execute the docker-compose file: ```docker-compose up --build``` <br />
This operation can take several minutes if you don't download the php image previously. <br />
-Once the environmnent is built, you can see the project on your browser on ```http://localhost:8080``` <br />
-Choose your grid, then submit your response and you can see the result :).